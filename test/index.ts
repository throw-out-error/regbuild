import { expect } from "chai";
import { patterns } from "../src";

describe("Patterns", () => {
    it("absolute path", () => {
        expect(
            patterns.absolutePath.test("/etc/apache2/sites-enabled/test.conf")
        ).to.be.ok;
    });
    it("relative path", () => {
        expect(patterns.relativePath.test("./Documents/test.txt")).to.be.ok;
    });

    it("email", () => {
        expect(patterns.email.test("theoparis@joe.example.com")).to.be.ok;
    });

    it("url", () => {
        expect(
            patterns
                .uri()
                .test("https://joe.example.website.com:8080/1/2/3/4?q&d")
        ).to.be.ok;
    });

    it("mongodb uri", () => {
        expect(
            patterns
                .uri(["mongodb"])
                .test("mongodb://joe:1234@db.example.com:255654/test")
        ).to.be.ok;
    });
});
