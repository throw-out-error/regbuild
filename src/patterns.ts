export const absolutePath = /^\/[^/].*/i;

export const relativePath = /^[^/].*/i;

export const path = /^\/?[^/].*/i;

export const email = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const uri = (protocols: string[] = ["https?"]) =>
    new RegExp(
        `(?:(?:${protocols.join(
            "|"
        )}):\/\/|www\.)(([\s\S]*:[\s\S]*)?@([\s\S]*))?(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])`,
        "gim"
    );
