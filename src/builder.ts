export enum RegexpGroup {
    Capture,
    NonCapture,
    PositiveLookahead,
    NegativeLookahead,
}

export type Regex = RegExp | string;
export type RegexExpression = Regex | RegexBuilder;

export const expressionToString = (exp: RegexExpression) => {
    if (exp instanceof RegExp)
        return exp.source.substr(1, exp.source.length - 1);
    else return exp;
};

export class RegexBuilder {
    private _matchBegin = false;
    private _matchEnd = false;
    private _regStr = "";

    // ==================================== FLAGS ====================================
    private _flags: Record<string, { value: boolean; flag: string }> = {
        globalSearch: { value: false, flag: "g" },
        caseInsensitive: { value: false, flag: "i" },
        multiLineSearch: { value: false, flag: "m" },
        unicode: { value: false, flag: "u" },
        sticky: { value: false, flag: "y" },
    };

    globalSearch = () => {
        this._flags.globalSearch.value = true;
        return this;
    };
    caseInsensitive = () => {
        this._flags.caseInsensitive.value = true;
        return this;
    };
    multiLineSearch = () => {
        this._flags.multiLineSearch.value = true;
        return this;
    };
    unicode = () => {
        this._flags.unicode.value = true;
        return this;
    };
    sticky = () => {
        this._flags.sticky.value = true;
        return this;
    };

    /**
     * All characters are treated literally except for the control characters
     * and following special characters
     */
    // private _specialcharacters = [
    //   '\\', '/', '[', ']', '(', ')', '{', '}', '?', '+', '*', '|', '.', '^', '$',
    // ];

    matchBegin = (): RegexBuilder => {
        this._matchBegin = true;
        return this;
    };
    matchEnd = (): RegexBuilder => {
        this._matchEnd = true;
        return this;
    };

    // ==================================== QUANTIFIER ====================================

    zeroOrMoreOf = (expression: RegexExpression): RegexBuilder => {
        this._regStr += `${this._addParenthesesIfNeed(expression)}*`;
        return this;
    };

    oneOrMoreOf = (expression: RegexExpression): RegexBuilder => {
        this._regStr += `${this._addParenthesesIfNeed(expression)}+`;
        return this;
    };

    optional = (expression: RegexExpression): RegexBuilder => {
        this._regStr += `${this._addParenthesesIfNeed(expression)}?`;
        return this;
    };

    /**
     * A regexp contains one or more string sequences.
     * The sequences are separated by the | (vertical bar) character.
     * The choice matches if any of the sequences match.
     * It attempts to match each of the sequences in order.
     *
     * The regex : `|`
     */
    anyOf = (choices: RegexExpression[]): RegexBuilder => {
        this._regStr += `(${choices.join("|")})`;
        return this;
    };

    /**
     * The regex : `xx{n}`
     */
    nTimesOf = (expression: RegexExpression, count: number): RegexBuilder => {
        this._regStr += `${expression}{${count}}`;
        return this;
    };

    /**
     * N times of expression or more
     * The regex : `xx{n,}`
     */
    moreThanNTimesOf = (
        expression: RegexExpression,
        count: number
    ): RegexBuilder => {
        this._regStr += `${expression}{${count},}`;
        return this;
    };

    expressionTimesBetween = (
        expression: RegexExpression,
        from: number,
        to: number
    ): RegexBuilder => {
        this._regStr += `${expression}{${from},${to}}`;
        return this;
    };

    /**
     * Any single character
     *
     * The regex: `.`
     */
    anySingleCharacter = (): RegexBuilder => {
        this._regStr += ".";
        return this;
    };

    // ==================================== ESCAPE ====================================

    /**
     * Formfeed character
     *
     * The regex: `\f`
     */
    formfeed = (): RegexBuilder => {
        this._regStr += "\\f";
        return this;
    };

    /**
     * Newline character
     * The regex: '\n'
     */
    newline = (): RegexBuilder => {
        this._regStr += "\\n";
        return this;
    };

    /**
     * Carriage return character
     * The regex: '\r'
     */
    carriageReturn = (): RegexBuilder => {
        this._regStr += "\\r";
        return this;
    };

    /**
     * Tab character
     * The regex: '\t'
     */
    tab = (): RegexBuilder => {
        this._regStr += "\\t";
        return this;
    };

    // TODO:
    // escapeHexadecimalDigits = () => { this._regStr += '\\u'; return this; };
    // a = () => { this._regStr += '\\u'; return this; }; // \b & \B

    /**
     * A single digit
     *
     * The regex: `\d` the same as `[0-9]`
     */
    anyDigit = (): RegexBuilder => {
        this._regStr += "\\d";
        return this;
    };

    /**
     * Not a digit
     *
     * The regex : `\D` same as `[^0-9]`
     */
    anyNonDigit = (): RegexBuilder => {
        this._regStr += "\\D";
        return this;
    };

    /**
     * A partial set of Unicode whitespace characters
     *
     * The regex : `\s` same as `[\f\n\r\t\u000B\u0020\u00A0\u2028\u2029]`
     */
    whitespace = (): RegexBuilder => {
        this._regStr += "\\s";
        return this;
    };

    /**
     * Not whitespace
     *
     * The regex : `\S` same as `[^\f\n\r\t\u000B\u0020\u00A0\u2028\u2029]`
     */
    notAWhitespace = (): RegexBuilder => {
        this._regStr += "\\S";
        return this;
    };

    /**
     * Any word character (letter, number, underscore)
     *
     * The regex : `\w` same as `[0-9A-Z_a-z]`
     */
    anyWordCharacter = (): RegexBuilder => {
        this._regStr += "\\w";
        return this;
    };

    /**
     * Any non-word character
     *
     * The regex : `\W` same as `[^0-9A-Z_a-z]`
     */
    anyNonWordCharacter = (): RegexBuilder => {
        this._regStr += "\\W";
        return this;
    };

    // TODO: \1 \2 \3 \...

    // ==================================== GROUP ====================================

    /**
     * The characters that match the group will be captured.
     * Every capture group is given a number.
     * The first capturing ( in the regular expression is group 1.
     * The second capturing ( in theregular expression is group 2.
     *
     * The regex : `(xxx)`
     */
    capture = (expression: RegexExpression): RegexBuilder =>
        this._group(expression, RegexpGroup.Capture);

    /**
     * A noncapturing group has a (?: prefix.
     * A noncapturing group simply matches;
     * it does not capture the matched text.
     * This has the advantage of slight faster performance.
     * Noncapturing groups do not interfere with the numbering of capturing groups.
     *
     * The regex : `(?:)`
     */
    nonCapture = (expression: RegexExpression): RegexBuilder =>
        this._group(expression, RegexpGroup.NonCapture);

    /**
     * A positive lookahead group has a (?= prefix.
     * It is like a noncapturing group except that after the group matches,
     * the text is rewound to where the group started, effectively matching nothing.
     *
     * The regex : `(?=xxx)`
     */
    positiveLookahead = (expression: RegexExpression): RegexBuilder =>
        this._group(expression, RegexpGroup.PositiveLookahead);

    /**
     * A negative lookahead group has a (?! prefix.
     * It is like a positive lookahead group,
     * except that it matches only if it fails to match
     *
     * The regex : `(?!xxx)`
     */
    negativeLookahead = (expression: RegexExpression): RegexBuilder =>
        this._group(expression, RegexpGroup.NegativeLookahead);

    // ==================================== Class ====================================
    // like [...]

    /**
     * Demo: aSingleCharacterInString('abc') represent A single character of: a, b, or c
     * It will generate `[abc]`
     */
    aSingleCharacterIn = (str: RegexExpression): RegexBuilder => {
        this._regStr += `[${str}]`;
        return this;
    };

    append(str: RegexExpression) {
        this._regStr += expressionToString(str);
        return this;
    }

    clone(): RegexBuilder {
        return new RegexBuilder().append(this.build());
    }

    /**
     * Demo anySingleCharacterExcept('abc') represent Any single character except: a, b, or c
     * It will generate `[^abc]`
     */
    anySingleCharacterExcept = (str: string): RegexBuilder => {
        this._regStr += `[^${str}]`;
        return this;
    };

    // TODO: [a - z] [a-zA-Z] [0-9]

    build = (): RegExp => {
        return new RegExp(this.toString(), this._allFlags());
    };

    buildString = (): string => `${this.toString()}/${this._allFlags()}`;

    debug = (
        logger: { log: (msg: unknown, ...args: unknown[]) => unknown } = console
    ): RegexBuilder => {
        logger.log(this.toString());
        return this;
    };

    toString = (): string => this._generateRegexpCoreStr();

    _generateRegexpCoreStr = () => {
        let result = "";
        if (this._matchBegin) result += "^";
        result += this._regStr;
        if (this._matchEnd) result += "$";
        return result;
    };

    private _allFlags = () => {
        let result = "";
        for (const key in this._flags)
            if (this._flags[key].value) result += this._flags[key].flag;

        return result;
    };

    private _addParenthesesIfNeed = (expression: RegexExpression): string => {
        if (expression instanceof RegExp) expression = expression.source;
        else if (expression instanceof RegexBuilder)
            expression = expression.build().source;

        return expression.length > 1 ? `(?:${expression})` : expression;
    };

    private _group = (
        expression: RegexExpression,
        type: RegexpGroup
    ): RegexBuilder => {
        switch (type) {
            case RegexpGroup.Capture:
                this._regStr += `(${expression})`;
                break;
            case RegexpGroup.NonCapture:
                this._regStr += `(?:${expression})`;
                break;
            case RegexpGroup.PositiveLookahead:
                this._regStr += `(?=${expression})`;
                break;
            case RegexpGroup.NegativeLookahead:
                this._regStr += `(?!${expression})`;
                break;
        }
        return this;
    };
}

/**
 * Runs each regex in the input array on the input text and replaces the match with the second array value.
 * <br>
 * Example: replaceAll("hello name", [["name", "Joe"]])
 */
export const replaceAll = (
    inputText: string,
    regex: Array<[RegexExpression, string]>
): string => {
    let result = inputText;
    for (const r of regex)
        result = inputText.replace(
            r[0] instanceof RegexBuilder ? r[0].build() : r[0],
            r[1]
        );
    return result;
};

export const parseQuoted = (str: string): string[] => {
    const params = [];
    const regex = /((['"`])*?\2|\S+)\s*/y;
    /*    const regex1 = new RegexBuilder().capture(
        new RegexBuilder().capture(
            new RegexBuilder().aSingleCharacterInString(`'"\``)
        )
    ); */
    let index = regex.lastIndex;

    while (regex.exec(str)) {
        regex.lastIndex = index;
        params.push(regex.exec(str)[1]);
        index = regex.lastIndex;
    }

    return params;
};
